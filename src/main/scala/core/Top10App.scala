package core


import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
/**

  20%+15%
  1. 試著從dataset/small-ratings.csv 計算每部電影ID的平均評價
  2. 試著利用movieId 從dataset/movie.csv join出title
  3. 試著印出評價最高的前10名電影，格式符合(movieId, average_rating, title )
  4. (加分題30%)試著從dataset/small-ratings.csv 計算每部電影ID的評價的平均數及標準差

  */
object Top10App extends App{
  val conf = new SparkConf().setAppName("top10-ratings")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

/**
  1. 試著從dataset/small-ratings.csv 計算每部電影ID的平均評價(10%)
  small-ratings.csv 格式如下
  userId, movieId, rating,  timestamp
  107799, 30707,   4.5,     1128005714
  28398,  3701,    1.5,     1075601839
  131283, 1281,    4.0,     947176194
  76688,  4427,    4.5,     1238740344

  提示:
  僅取出movieId及rating兩個欄位即可
  */



  val ratings:RDD[(Int,Double)]={

    val ratingMaps =   sc.textFile("dataset/small-ratings.csv")
      .map(_.split(","))
      .filter(_(0)!="userId")
      .map(strs=>{
      (
        strs(1).toInt,    // movieId
        strs(2).toDouble  // rating
      )
    })

  val ratingMeans = ratingMaps.mapValues(rating => (rating,1))
    .reduceByKey((acc,curr) => (acc._1+curr._1) -> (acc._2+curr._2))
    .mapValues(v => (v._1 / v._2).toDouble )

 // ratingMeans.foreach(println)

  ratingMeans


  }


  /**
   RDD[(movieId,(average,count))]
   建議實現averageAndCountRatingByMovieId，可幫助你完成4. 加分題！！
   沒實作也沒關係，不影響分數
    */
//val averageAndCountRatingByMovieId: RDD[(Int, (Double, Int))] =


  // RDD[(movieId,averageRating)]
  //val averageRatingByMovieId:RDD[(Int,Double)]= ???


  /**
  2. 試著利用movieId 從dataset/movies.csv join出title(5%)
  movies.csv 格式如下
  movieId,  title,                    genres
  1,        Toy Story (1995),         Adventure|Animation|Children|Comedy|Fantasy
  2,        Jumanji (1995),           Adventure|Children|Fantasy
  3,        Grumpier Old Men (1995),  Comedy|Romance
  4,        Waiting to Exhale (1995), Comedy|Drama|Romance


  提示:
  僅取出movieId及title兩個欄位即可
    */

  val movies: RDD[(Int, String)] ={

    val movies =   sc.textFile("dataset/movies.csv")
      .map(_.split(","))
      .filter(_(0)!="movieId")
      .map(strs=>{
        (
          strs(0).toInt,    // movieId
          strs(1).toString  // title
        )
      })


    movies

  }


  val joined: RDD[(Int, (Double, String))]= ratings.join(movies)
  //joined.foreach(println)

  /**
  3. 試著印出評價最高的前10名電影，格式符合(movieId, average_rating, title ) (5%)
  提示:
  排序後再取出前10名
    */
  val top10: Array[(Int, (Double, String))] = joined.sortBy(_._2._1,false).take(10)

  top10.foreach(println)

  /**
  4. (加分題15%)試著從dataset/small-ratings.csv 計算每部電影ID的評價的敘述統計量(stats)
  small-ratings.csv 格式如下
  userId, movieId, rating,  timestamp
  107799, 30707,   4.5,     1128005714
  28398,  3701,    1.5,     1075601839
  131283, 1281,    4.0,     947176194
  76688,  4427,    4.5,     1238740344

  提示:
  僅取出movieId及rating兩個欄位即可
    */

  //Stats(平均數,母體變異數)
  //變異數公式 http://estat.ncku.edu.tw/topic/desc_stat/base/variance.html
  case class Stats(mean:Double,variance:Double)

  // RDD[(movieId,stats )]
  val statsRatingByMovieId:RDD[(Int,Stats)]= ???
}










