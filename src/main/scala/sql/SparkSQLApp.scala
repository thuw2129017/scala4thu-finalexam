package sql

import org.apache.spark.sql.SparkSession

/**
  * Created by mark on 04/06/2017.
  */

/**
  請以SparkSQL完成以下題目(70%)
 */
object SparkSQLApp extends App{

  val ss = SparkSession
    .builder()
    .master("local[*]")
    .appName("Spark SQL first example")
    .getOrCreate()

/**
1. 根據以下題目敘述，完成???部份 (10%)
*/
  /**
  1.1 請讀取dataset/small-ratings.csv, 並創建一個名為ratings的TempView(Table) (5%)

    */

  var ratingsSrc=ss.read.option("header", "true").csv("/Users/mac051/Downloads/scala4thu-finalexam/dataset/small-ratings.csv")

  ratingsSrc.createOrReplaceTempView("ratings")

  /**
  1.2 請讀取dataset/movies.csv, 並創建一個名為movies的TempView(Table) (5%)

    */

  var moviesSrc=ss.read.option("header", "true").csv("/Users/mac051/Downloads/scala4thu-finalexam/dataset/movies.csv")

  moviesSrc.createOrReplaceTempView("movies")

/**
2. 根據以下題目敘述，使用SQL語法完成以下???部份(60%)
*/
  /**
  2.1 試著從small-ratings.csv中，根據timestamp做降序排列，並show與以下欄位相同的資料格式(10%)
  +------+-------+------+----------+
  |userId|movieId|rating| timestamp|
  +------+-------+------+----------+
  */
  ss.sql("select userId,movieId,rating, timestamp from ratings order by timestamp DESC").show()

  /**
  2.2 試著從small-ratings.csv中，計算每部電影的平均rating, 並show與以欄位相同的資料(10%)
  +-------+------------------+
  |movieId|              mean|
  +-------+------------------+

    */
  ss.sql("select movieId,avg(rating) as mean from ratings group by movieId").show()

  /**
  2.3 試著從small-ratings.csv中，計算出每部電影平均rating並根據平均rating做降序排列，
  並show與以下欄位相同的資料格式(10%)
  +-------+------------------+
  |movieId|              mean|
  +-------+------------------+

    */
  ss.sql("select movieId,avg(rating) as mean from ratings group by movieId order by mean DESC" ).show()
  /**
  2.4 試著從small-ratings.csv中，找出movieId為30707且userId為107799，並show與以下欄位相同的資料格式(10%)
  +------+-------+------+----------+
  |userId|movieId|rating| timestamp|
  +------+-------+------+----------+


    */
  ss.sql("select * from ratings where movieId = 30707 and userId=107799").show()
  /**
  試著從movie.csv中，找出movieId為10的資料, 並show與以欄位相同的資料(10%)
  +-------+------------------+
  |movieId|              title|
  +-------+------------------+


    */
  ss.sql("select movieId,title from movies where movieId = 10").show()

/**
  2.6 試著從small-ratings.csv及movies.csv兩張表Join起來，以timestamp降序排列，
  並show出與以下欄位相同的資料格式(10%)
  +-------+--------------------+------+---------+
  |movieId|               title|rating|timestamp|
  +-------+--------------------+------+---------+
  */
  ss.sql("select m.movieId,m.title,r.rating,r.timestamp from ratings r join movies m where m.movieId = r.movieId order by r.timestamp").show()

}
